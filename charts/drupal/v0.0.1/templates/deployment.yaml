apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "drupal.fullname" . }}
  labels:
    app: "{{ template "drupal.fullname" . }}"
    chart: "{{ template "drupal.chart" . }}"
    release: {{ .Release.Name | quote }}
    heritage: {{ .Release.Service | quote }}
spec:
  selector:
    matchLabels:
      app: "{{ template "drupal.fullname" . }}"
      release: {{ .Release.Name | quote }}
  {{- if .Values.updateStrategy }}
  strategy: {{ toYaml .Values.updateStrategy | nindent 4 }}
  {{- end }}
  replicas: {{ .Values.replicaCount }}
  template:
    metadata:
      labels:
        app: "{{ template "drupal.fullname" . }}"
        chart: "{{ template "drupal.chart" . }}"
        release: {{ .Release.Name | quote }}
{{- if or .Values.podAnnotations .Values.metrics.enabled }}
      annotations:
{{- if .Values.podAnnotations }}
{{ toYaml .Values.podAnnotations | indent 8 }}
{{- end }}
{{- if .Values.metrics.podAnnotations }}
{{ toYaml .Values.metrics.podAnnotations | indent 8 }}
{{- end }}
{{- end }}
    spec:
      {{- if .Values.schedulerName }}
      schedulerName: "{{ .Values.schedulerName }}"
      {{- end }}
{{- include "drupal.imagePullSecrets" . | indent 6 }}
      hostAliases:
      - ip: "127.0.0.1"
        hostnames:
        - "status.localhost"
      containers:
      - name: drupal
        image: {{ template "drupal.image" . }}
        imagePullPolicy: {{ .Values.image.pullPolicy | quote }}
        env:
        - name: ALLOW_EMPTY_PASSWORD
          value: {{ ternary "yes" "no" .Values.allowEmptyPassword | quote }}
        - name: MARIADB_HOST
        {{- if .Values.mariadb.enabled }}
          value: {{ template "mariadb.fullname" . }}
        {{- else }}
          value: {{ .Values.externalDatabase.host | quote }}
        {{- end }}
        - name: MARIADB_PORT_NUMBER
        {{- if .Values.mariadb.enabled }}
          value: "3306"
        {{- else }}
          value: {{ .Values.externalDatabase.port | quote }}
        {{- end }}
        - name: DRUPAL_DATABASE_NAME
        {{- if .Values.mariadb.enabled }}
          value: {{ .Values.mariadb.db.name | quote }}
        {{- else }}
          value: {{ .Values.externalDatabase.database | quote }}
        {{- end }}
        - name: DRUPAL_DATABASE_USER
        {{- if .Values.mariadb.enabled }}
          value: {{ .Values.mariadb.db.user | quote }}
        {{- else }}
          value: {{ .Values.externalDatabase.user | quote }}
        {{- end }}
        - name: DRUPAL_DATABASE_PASSWORD
          valueFrom:
            secretKeyRef:
            {{- if .Values.mariadb.enabled }}
              name: {{ template "mariadb.fullname" . }}
              key: mariadb-password
            {{- else }}
              name: {{ printf "%s-%s" .Release.Name "externaldb" }}
              key: db-password
            {{- end }}
        - name: DRUPAL_USERNAME
          value: {{ .Values.drupalUsername | quote }}
        - name: DRUPAL_PASSWORD
          valueFrom:
            secretKeyRef:
              name: {{ template "drupal.fullname" . }}
              key: drupal-password
        - name: DRUPAL_EMAIL
          value: {{ .Values.drupalEmail | quote }}
        - name: DRUPAL_FIRST_NAME
          value: {{ .Values.drupalFirstName | quote }}
        - name: DRUPAL_LAST_NAME
          value: {{ .Values.drupalLastName | quote }}
        - name: DRUPAL_HTACCESS_OVERRIDE_NONE
          value: {{ ternary "yes" "no" .Values.allowOverrideNone | quote }}
        - name: DRUPAL_BLOG_NAME
          value: {{ .Values.drupalBlogName | quote }}
        - name: DRUPAL_SKIP_INSTALL
          value: {{ ternary "yes" "no" .Values.drupalSkipInstall | quote }}
        - name: DRUPAL_TABLE_PREFIX
          value: {{ .Values.drupalTablePrefix | quote }}
        - name: DRUPAL_SCHEME
          value: {{ .Values.drupalScheme | quote }}
        ports:
        - name: http
          containerPort: 80
        - name: https
          containerPort: 443
        volumeMounts:
        - mountPath: /bitnami/drupal
          name: drupal-data
          subPath: drupal
        {{- if and .Values.allowOverrideNone .Values.customHTAccessCM}}
        - mountPath: /opt/bitnami/drupal/drupal-htaccess.conf
          name: custom-htaccess
          subPath: drupal-htaccess.conf
        {{- end }}
        resources:
{{ toYaml .Values.resources | indent 10 }}
{{- if .Values.metrics.enabled }}
      - name: metrics
        image: {{ template "drupal.metrics.image" . }}
        imagePullPolicy: {{ .Values.metrics.image.pullPolicy | quote }}
        command: [ '/bin/apache_exporter', '-scrape_uri', 'http://status.localhost:80/server-status/?auto']
        ports:
        - name: metrics
          containerPort: 9117
        livenessProbe:
          httpGet:
            path: /metrics
            port: metrics
          initialDelaySeconds: 15
          timeoutSeconds: 5
        readinessProbe:
          httpGet:
            path: /metrics
            port: metrics
          initialDelaySeconds: 5
          timeoutSeconds: 1
        resources:
{{ toYaml .Values.metrics.resources | indent 10 }}
{{- end }}
{{- if .Values.sidecars }}
{{ toYaml .Values.sidecars | indent 6 }}
{{- end }}
      volumes:
      {{- if and .Values.allowOverrideNone .Values.customHTAccessCM}}
      - name: custom-htaccess
        configMap:
          name: {{ template "drupal.customHTAccessCM" . }}
      {{- end }}
      - name: drupal-data
      {{- if .Values.persistence.enabled }}
        persistentVolumeClaim:
          claimName: {{ .Values.persistence.existingClaim | default (include "drupal.fullname" .) }}
      {{- else }}
        emptyDir: {}
      {{ end }}
    {{- if .Values.nodeSelector }}
      nodeSelector:
{{ toYaml .Values.nodeSelector | indent 8 }}
      {{- end -}}
    {{- with .Values.affinity }}
      affinity:
{{ toYaml . | indent 8 }}
    {{- end }}
    {{- with .Values.tolerations }}
      tolerations:
{{ toYaml . | indent 8 }}
    {{- end }}
